﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AbstractFactoryConsole.Models
{
    public class CdiscountOperator : MarketplaceOperator
    {

        public override string GetOrder(string idOrder)
        {
            return "Cdiscount " + idOrder;
        }

        public override string SendOrder(string idOrder)
        {
            return "Send to Cdiscount " + idOrder;
        }
    }
}
