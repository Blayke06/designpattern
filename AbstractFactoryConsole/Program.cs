﻿using AbstractFactoryConsole.Factory;
using AbstractFactoryConsole.Models;
using System;

namespace AbstractFactoryConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            MarketplaceOperator marketplaceOperator;
            switch (args[0])
            {
                case "Amazon":
                    marketplaceOperator = new MarketplaceOperatorFactory().CreateMarketplaceOperator(new AmazonOperatorFactory());
                    break;
                case "Cdiscount":
                    marketplaceOperator = new MarketplaceOperatorFactory().CreateMarketplaceOperator(new CdiscountOperatorFactory());
                    break;
                case "Rakuten":
                    marketplaceOperator = new MarketplaceOperatorFactory().CreateMarketplaceOperator(new RakutenOperatorFactory());
                    break;
                default:
                    throw new MarketplaceOperatorException();
            }

            new Process(marketplaceOperator).DoWork();
        }
    }
}
