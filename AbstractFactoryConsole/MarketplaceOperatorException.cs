﻿using System;
using System.Runtime.Serialization;

namespace AbstractFactoryConsole
{
    [Serializable]
    internal class MarketplaceOperatorException : Exception
    {
        public MarketplaceOperatorException()
        {
        }

        public MarketplaceOperatorException(string message) : base(message)
        {
        }

        public MarketplaceOperatorException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected MarketplaceOperatorException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}