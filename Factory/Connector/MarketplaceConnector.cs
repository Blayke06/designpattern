﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Factory.Connector
{
    /// <summary>
    /// Product
    /// </summary>
    public abstract class MarketplaceConnector
    {
        public abstract string Url { get;}
        public abstract int Number { get; set; }

        public MarketplaceConnector(int number)
        {
            Number = number;
        }

        public abstract void Operation();

        public void LogConsoleInfo()
        {
            Console.WriteLine($"Do some operation of {this.GetType().Name} Url : {Url}");
        }

    }
}
