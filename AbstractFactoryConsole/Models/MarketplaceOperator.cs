﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AbstractFactoryConsole.Models
{
   public abstract class MarketplaceOperator
    {
        public abstract string GetOrder(string idOrder);
        public abstract string SendOrder(string idOrder);

    }
}
