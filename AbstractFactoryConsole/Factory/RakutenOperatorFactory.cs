﻿using AbstractFactoryConsole.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace AbstractFactoryConsole.Factory
{
    public class RakutenOperatorFactory : IMarketplaceOperatorAbstractFactory
    {
        public MarketplaceOperator CreateOperator()
        {
            return new RakutenOperator();
        }
    }
}
