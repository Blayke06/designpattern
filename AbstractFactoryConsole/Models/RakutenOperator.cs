﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AbstractFactoryConsole.Models
{
    public class RakutenOperator : MarketplaceOperator
    {

        public override string GetOrder(string idOrder)
        {
            return "RakutenOperator " + idOrder;
        }

        public override string SendOrder(string idOrder)
        {
            return "Send to RakutenOperator " + idOrder;
        }
    }
}
