﻿using Factory.Connector;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Text;

namespace Factory
{
    /// <summary>
    /// Product B
    /// </summary>
    public class RakutenConnector : MarketplaceConnector
    {
        public override string Url { get => ConfigurationManager.AppSettings["RakutenUrl"] + @$"\{Number}" ; }
        public override int Number { get; set; }

        public RakutenConnector(int number) : base(number)
        {
            Number = number;
        }

        public override void Operation()
        {
            this.LogConsoleInfo();
            Console.WriteLine($"OPERATION RAKUTEN");
            Console.WriteLine($"Je suis au toilette car j'ai envie de faire du deltaplane\n");
        }
    }
}
