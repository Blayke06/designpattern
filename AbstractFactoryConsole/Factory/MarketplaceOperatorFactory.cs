﻿using AbstractFactoryConsole.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace AbstractFactoryConsole.Factory
{
    public class MarketplaceOperatorFactory
    {
        public MarketplaceOperator CreateMarketplaceOperator(IMarketplaceOperatorAbstractFactory marketplaceOperatorAbstractFactory)
        {
            return marketplaceOperatorAbstractFactory.CreateOperator();
        }
    }
}
