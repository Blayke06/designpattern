﻿using AbstractFactoryConsole.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace AbstractFactoryConsole.Factory
{
    public class CdiscountOperatorFactory : IMarketplaceOperatorAbstractFactory
    {
        public MarketplaceOperator CreateOperator()
        {
            return new CdiscountOperator();
        }
    }
}
