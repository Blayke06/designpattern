# Design pattern
Ce projet a pour but de répertorier tous les design pattern appris

- Factory

Le design pattern Factory, ou Fabrique est un design pattern permettant de séparer la création d'objets dérivant d'une classe mère de leur utilisation. De ce fait, on a alors la possibilité de créer plusieurs objets issue d'une même classe mère.

Le modèle Factory Method est largement utilisé. Il est très utile lorsque vous devez fournir un niveau élevé de flexibilité pour votre code.

Identification : Les méthodes d'usine peuvent être reconnues par les méthodes de création, qui créent des objets à partir de classes concrètes, mais les renvoient sous forme d'objets de type abstrait ou d'interface.

1. Une fabrique générique : Elle contient toutes les méthodes nécessaires à la création d'un produit
2. Une fabrique : Elle va créer le produit souhaité
3. Un produit : Le produit créé par la fabrique, dérivant du produit générique
4. Un produit générique : Le produit d'origine, contenant toutes les méthodes permettant de réaliser les actions associées

![alt text](https://www.jmdoudoux.fr/java/dej/images/dp004.png "Design pattern Factory schema")

- Abstract Factory