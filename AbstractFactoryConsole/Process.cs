﻿using AbstractFactoryConsole.Factory;
using AbstractFactoryConsole.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace AbstractFactoryConsole
{
    public class Process
    {
        public MarketplaceOperator MarketplaceOperator { get; set; }
        public Process(MarketplaceOperator marketplaceOperator)
        {
            MarketplaceOperator = marketplaceOperator;
        }

        public void DoWork()
        {
            var order = MarketplaceOperator.GetOrder("toto");

            Console.WriteLine(MarketplaceOperator.SendOrder(order));
        }
    }
}
