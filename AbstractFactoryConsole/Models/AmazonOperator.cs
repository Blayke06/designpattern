﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AbstractFactoryConsole.Models
{
    public class AmazonOperator : MarketplaceOperator
    {

        public override string GetOrder(string idOrder)
        {
            return "Amazon " + idOrder;
        }

        public override string SendOrder(string idOrder)
        {
            return "Send to Amazon " + idOrder;
        }
    }
}
