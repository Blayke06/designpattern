﻿using AbstractFactoryConsole.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace AbstractFactoryConsole.Factory
{
    public interface IMarketplaceOperatorAbstractFactory
    {
        public MarketplaceOperator CreateOperator();
    }
}
