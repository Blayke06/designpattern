﻿using Factory.Connector;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Text;

namespace Factory.Factory
{
    /// <summary>
    /// Factory A
    /// </summary>
    public class AmazonFactory : MarketplaceFactory
    {
        public AmazonFactory(int number):base(number)
        {
            Number = number;
        }

        public override MarketplaceConnector CreateConnector() => new AmazonConnector(Number);
    }
}
