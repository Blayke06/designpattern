﻿using AbstractFactoryConsole.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace AbstractFactoryConsole.Factory
{
    public class AmazonOperatorFactory : IMarketplaceOperatorAbstractFactory
    {
        public MarketplaceOperator CreateOperator()
        {
            return new AmazonOperator();
        }
    }
}
