﻿using Factory.Connector;
using System;
using System.Collections.Generic;
using System.Text;

namespace Factory.Factory
{
    /// <summary>
    /// Factory
    /// </summary>
    public abstract class MarketplaceFactory
    {
        public int Number { get; set; }

        public MarketplaceFactory(int number)
        {
            Number = number;
        }

        //Polymorphisme
        public abstract MarketplaceConnector CreateConnector();
    }
}
