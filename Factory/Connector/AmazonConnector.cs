﻿using Factory.Connector;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Text;

namespace Factory
{
    /// <summary>
    /// Product A
    /// </summary>
    public class AmazonConnector : MarketplaceConnector
    {
        public override string Url { get => ConfigurationManager.AppSettings["AmazonUrl"] + @$"\{Number}"; }
        public override int Number { get; set; }

        public AmazonConnector(int number):base(number)
        {
            Number = number;
        }


        public override void Operation()
        {
            this.LogConsoleInfo();
            Console.WriteLine($"OPERATION AMAZON");
            Console.WriteLine($"Je fais la cuisine par ce qu'au fond de moi je suis un radis numero {Number}");
            Console.WriteLine("TotoCharlotte33 \n");
        }
    }
}
