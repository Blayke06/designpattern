﻿using Factory.Connector;
using Factory.Factory;
using System;
using System.Collections.Generic;

namespace Factory
{
    class Program
    {
        static void Main(string[] args)
        {

            // Instanciation des factories in List<AbstractFactory>
            List<MarketplaceFactory> marketplaceFactories = new List<MarketplaceFactory>()
            {
                new AmazonFactory(1),
                new RakutenFactory(1),
            };
            
            //Exemple avec argument
            switch (args[0])
            {
                case "Amazon":
                    marketplaceFactories.Add(new AmazonFactory(2));
                    break;
                case "Rakuten":
                    marketplaceFactories.Add(new RakutenFactory(2));
                    break;
                default:
                    break;
            }

            //Détermination à l'exécution du type d'objet
            foreach (var factory in marketplaceFactories)
            {
                MarketplaceConnector marketplaceConnector = factory.CreateConnector();
                marketplaceConnector.Operation();
            }

            Console.ReadKey();
        }
    }
}
