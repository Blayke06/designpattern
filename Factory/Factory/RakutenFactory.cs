﻿using Factory.Connector;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Text;

namespace Factory.Factory
{
    /// <summary>
    /// Factory B
    /// </summary>
    public class RakutenFactory : MarketplaceFactory
    {
        public RakutenFactory(int number) : base(number)
        {
            Number = number;
        }

        public override MarketplaceConnector CreateConnector() => new RakutenConnector(Number);
    }
}
